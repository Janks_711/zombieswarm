﻿/*

Humainoid Actor Script
02/03/2017 v1.0
Created by Michael Kijanka
Academyof interactive Entertainment

*/


// This class serves as the parent class for all Humanoid actors

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanoidActor : MonoBehaviour {

    [Header("Vitals")]
    public float health; // Health Variable
    public float maxHealth = 100; // maximum health
    public int damage; // Damage output
    public float movementSpeed; //Speed of movement

    protected Animator animator; // Get character animator
    protected bool isDead = false; // Check if Humanoid is dead

    private int count = 0; // Count to stop getting flooded with messages

    // Use this for initialization
    void Start () {
        
    }

    // Update is called once per frame
    void Update () {

    }

    virtual protected void Move() { } // Move function for the Humanoid

    virtual protected void Death()// do something when actor dies
    {
        if (count == 0)
        {
            Debug.Log(this.gameObject.ToString() + " has DIED");
            count = 1;
        }
        animator.SetBool("bIsDead", true);
    } 

    virtual public void TakeDamage(int dmgTaken) // Take Damage
    {
        if (isDead == false)
        {
            health -= dmgTaken;
            Debug.Log(this.gameObject.ToString() + " has taken " + dmgTaken + " damage. " + health + " health remaining");
        }
    } 
}
