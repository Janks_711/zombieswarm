﻿/*

Spawn Actor Script
13/03/2017 v1.0
Created by Michael Kijanka
Academy of interactive Entertainment

*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is how the spawn points work. They should constantly spawn zombies
/// provided the maximum number of zombies havenot been reached.
/// </summary>

public class SpawnPointActor : MonoBehaviour {

    [Header("Enemies to Spawn")]
    public GameObject enemyPrefab; // The enemy that is bing spawned

    [Header("Timers")]
    public float spawnTime; // Time between spawns
    private float timer; // Timer for counting time between spawns

    public GameControl gameControl; // For determining if max spawns have been reached

	// Use this for initialization
	void Start () {
        gameControl = FindObjectOfType<GameControl>();
	}
	
	// Update is called once per frame
	void Update () {
        SpawnEnemy();
	}

    // Spawn Enemy
    public void SpawnEnemy()
    {
        if (enemyPrefab != null && gameControl.currentZombieCount < gameControl.maxZombieCount)
        {
            timer -= Time.deltaTime; // Count down the spawn Timer

            if (timer < 0)
            {
                Vector3 spawnPoint; // Where the enemy spawns
                
                spawnPoint = transform.position; // Set spawn point as this position
                Instantiate(enemyPrefab, spawnPoint, Quaternion.identity); // Create enemy
                gameControl.currentZombieCount++;
                timer = spawnTime; // reset timer
            }
        }
        else
            Debug.Log("No enemy Selected");

    }
}
