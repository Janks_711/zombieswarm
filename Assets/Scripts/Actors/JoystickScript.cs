﻿/*

JoyStick Script
12/03/2017 v1.0
Created by Michael Kijanka
Academy of interactive Entertainment

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// This class will define how the joysticks for the mobile platform will move and how they will work
/// </summary>

public class JoystickScript : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {

    private Image backgroundImage; // LargerJoystick Circle
    private Image joystickImg; // Smaller circle
    private Vector3 inputVector; //Get the current vector of user input

	// Use this for initialization
	void Start () {
        backgroundImage = GetComponent<Image>();
        joystickImg = transform.GetChild(0).GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //Define what happens when the user dragsthe joystick
    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos;

        // Check if the input from the user is on the background of the joystick, then return the position of the input
        // ped is the Point Event Data of where the user is pressing on the screen
        // Thenmove the joystick to the location of the input
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(backgroundImage.rectTransform, ped.position, ped.pressEventCamera, out pos))
        {
            pos.x = (pos.x / backgroundImage.rectTransform.sizeDelta.x);
            pos.y = (pos.y / backgroundImage.rectTransform.sizeDelta.y);

            inputVector = new Vector3(pos.x * 2 + 1, 0, pos.y * 2 - 1);
            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

            Debug.Log(inputVector);

            //Move Joystick Image
            joystickImg.rectTransform.anchoredPosition = new Vector3(inputVector.x * (backgroundImage.rectTransform.sizeDelta.x / 3.5f),
                                                                    inputVector.z * (backgroundImage.rectTransform.sizeDelta.y / 3.5f));
        }
    }

    // Check that the user has pressed on the screen
    public virtual void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }

    // Reset joystick position and input informationonce the user has stopped pressing on the screen
    public virtual void OnPointerUp(PointerEventData ped)
    {
        inputVector = Vector3.zero;
        joystickImg.rectTransform.anchoredPosition = Vector3.zero;
    }

    // Define Horizontal Axis
    public float Horizontal()
    {
        if (inputVector.x != 0)
            return inputVector.x;
        else
            return Input.GetAxis("Horizontal");
    }

    // Define Vertical Axis
    public float Vertical()
    {
        if (inputVector.z != 0)
            return inputVector.z;
        else
            return Input.GetAxis("Vertical");
    }
}
