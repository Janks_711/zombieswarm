﻿/*

Player Actor Script
02/03/2017 v1.0
Created by Michael Kijanka
Academy of interactive Entertainment

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Enum for the different weapon Types
public enum WEAPON
{
    PISTOL,
    SHOTGUN,
    RIFLE
};

// Defines a Player Actor Character

public class PlayerActor : HumanoidActor {

    private CharacterController controller; // Controller for the character

    public int armour = 0; // Armour for the character
    public float rotationSpeed; //Speed of character rotation
   
    private float movementSpeedPlaceHolder;
    private int playerScore = 0;

    [Header("Mobile Input")]
    public JoystickScript virtualJoyLeft; // Joystick Left
    public JoystickScript virtualJoyRight; // Joystick Right
    
    [Header("User Interface")]
    public Text scoreText = null;
    public Text ammoText = null;
    public Image weaponImage = null;
    public GameObject[] armourIcons;
    public Slider healthBar;

    [Header("Audio")]
    public AudioSource sfxAudioSource = null;
    //Audio Clips
    public AudioClip sfxDeath;
    public AudioClip[] sfxPickup;
    private int deathplaycount = 0;

    [Header("Weapons")]
    public Weapon currWeapon;
    public Weapon[] weaponList = null; // Put all weapons in this list
    public int fireRateIncrease = 0; //Divides the current firerate

    [Header("Player Buff")]
    public int buffTimer = 0; // How long buff should be applied for
    public float movementBuffIncrease = 0; //How much the movement speed should increase
    
    private bool buffApplied = false; //Check if buff has been applied

    // Use this for initialization
    void Start () {
        controller = gameObject.GetComponent<CharacterController>();
        animator = gameObject.GetComponent<Animator>();
        movementSpeedPlaceHolder = movementSpeed;

        health = maxHealth;

        if (healthBar != null)
            healthBar.value = CalculateHealth();

#if !UNITY_ANDROID
        if (virtualJoyLeft.gameObject == null || virtualJoyLeft.gameObject == null)
            Debug.Log("You forgot to attach the joysticks!");
        else
        {
            virtualJoyLeft.gameObject.SetActive(false);
            virtualJoyRight.gameObject.SetActive(false);
        }

#endif
    }

    // Update is called once per frame
    void Update () {

        if (scoreText != null)
            scoreText.text = "Score: " + playerScore.ToString();

        if (weaponImage != null)
            weaponImage.sprite = currWeapon.weapoonImage;

        if (healthBar != null)
            healthBar.value = CalculateHealth();

        if (ammoText != null)
        {
            if (currWeapon.AmmoRemaining() < 0)
                ammoText.text = "Ammo: Unlimited";
            else
                ammoText.text = "Ammo: " + currWeapon.AmmoRemaining();
        }
           

        //Check Health
        if (health <= 0)
            isDead = true;

        if (isDead == false)
        {

#if UNITY_ANDROID //Definemovement when on Android

        // Test fire animation
        if ((virtualJoyRight.Horizontal() !=0 || virtualJoyRight.Vertical() != 0) && Time.time > currWeapon.Nextfire)
        {
              currWeapon.Fire();
              animator.SetBool("isFiring_b", true);
              currWeapon.Nextfire = Time.time + currWeapon.fireRate;
            
        }
        else
        {
            animator.SetBool("isFiring_b", false);
        }


        JoyMove();
#else
            // Definemovement if not pn Android
            if (Input.GetAxis("Fire1") != 0 && Time.time > currWeapon.Nextfire)
            {
                currWeapon.Fire();
                animator.SetBool("isFiring_b", true);
                currWeapon.Nextfire = Time.time + currWeapon.fireRate;

            }
            else
            {
                animator.SetBool("isFiring_b", false);
            }

            Move(); 
#endif
            ArourIcons(); // ArmourIcons funciton

            // if buff Applies count down the timer until st
            if (buffApplied == true)
            {
                StartCoroutine(BuffTimer());
            }
        }

        else
        {
            if (sfxAudioSource != null && sfxDeath != null && deathplaycount == 0)
            {
                sfxAudioSource.PlayOneShot(sfxDeath);
                deathplaycount++;
            }
               
            Death();
        }
          
    }

    // Movement funnction if using the joysticks
    protected void JoyMove()
    {
        // Get Left Joystick movement
        float horizonatalMovement = virtualJoyLeft.Horizontal();
        float verticalMovement = virtualJoyLeft.Vertical();

        // Get Right Joystick Movement
        float rotationHorizontal = virtualJoyRight.Horizontal();
        float rotationVertical = virtualJoyRight.Vertical();

        // Set rotation Direction
        Vector3 rotationDirection = new Vector3(rotationHorizontal, 0, rotationVertical);

        // Define forwardsand backwards movements
        Vector3 moveDir = new Vector3(horizonatalMovement, 0, verticalMovement);
        moveDir = transform.TransformDirection(moveDir);
        moveDir *= movementSpeed;
        controller.Move(moveDir * Time.deltaTime);
        animator.SetFloat("speed_f", controller.velocity.magnitude);

        // Set animation
        animator.SetFloat("bVelX", horizonatalMovement);
        animator.SetFloat("bVelY", verticalMovement);

        // Turn Player to face direction
        this.transform.forward = Vector3.Lerp(transform.forward, rotationDirection, rotationSpeed * Time.deltaTime);

    }

    protected override void Move() // Define Default Movement for the Character
    {
        // Get input Axes
        float horizonatalMovement = Input.GetAxis("Horizontal");
        float verticalMovement = Input.GetAxis("Vertical");

        // Define forwards and backwards movements
        Vector3 moveDir = new Vector3(horizonatalMovement, 0, verticalMovement);
        moveDir = transform.TransformDirection(moveDir);
        moveDir *= movementSpeed;
        controller.Move(moveDir * Time.deltaTime);
        animator.SetFloat("speed_f", controller.velocity.magnitude);

        animator.SetFloat("bVelX", horizonatalMovement);
        animator.SetFloat("bVelY", verticalMovement);

        // Rotate player in fire direction
        Vector3 fireDirection = PlatformGetPlayerFireDirection();
        this.transform.forward = Vector3.Lerp(transform.forward, fireDirection, rotationSpeed * Time.deltaTime);

    }

    // Get players current firedirection
    private Vector3 PlatformGetPlayerFireDirection()
    {
        Vector3 mousePos = Input.mousePosition;

        //use the current camera to convert mouse position to a ray
        Ray mouseRay = Camera.main.ScreenPointToRay(mousePos);

        //Create a plane that faces up at the same position os the player
        Plane playerPlane = new Plane(Vector3.up, transform.position);

        // How far along the ray does the intersection with the plane occur?
        float rayDistance = 0;
        playerPlane.Raycast(mouseRay, out rayDistance);

        // Use the ray distance to calculate the point of collision
        Vector3 castPoint = mouseRay.GetPoint(rayDistance);

        Vector3 toCastPoint = castPoint - transform.position;
        toCastPoint.Normalize();

        return toCastPoint;
    }

    // Set new Weapon for the Player
    public void SetWeapon(WEAPON newWeapon)
    {
        if (weaponList != null)
        {
            switch (newWeapon)
            {
                case WEAPON.PISTOL:
                    currWeapon = weaponList[0];
                    break;
                case WEAPON.RIFLE:
                    currWeapon = weaponList[1];
                    break;
                case WEAPON.SHOTGUN:
                    currWeapon = weaponList[2];
                    break;

            }

        }
        // Reset the weapon, so the buff does not apply if switch out during the buff
        currWeapon.Reload();
        currWeapon.sfxreload.Play();
        ResetBuff();

        // Switch current Weapon image
        if(weaponImage != null && currWeapon.weapoonImage != null)
        {
            weaponImage.sprite = currWeapon.weapoonImage;
        }
    }

    // On picking up pickup play sound
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Armour")
            sfxAudioSource.PlayOneShot(sfxPickup[0]);

        if (collider.tag == "Health")
            sfxAudioSource.PlayOneShot(sfxPickup[1]);

        if (collider.tag == "Adrenaline")
            sfxAudioSource.PlayOneShot(sfxPickup[2]);
    }

    // Adrernaline Boost function
    // Increase fire rate of equipped weapon and increase movment speed
    public void ApplyAdrenalineBoost()
    {
        if (buffApplied == false)
        {
            movementSpeed += movementBuffIncrease;
            currWeapon.fireRate = currWeapon.fireRate / fireRateIncrease;
            buffApplied = true;
        }

    }

    // Function for limiting time user will be affected by andrenaline
    IEnumerator BuffTimer()
    {
        yield return new WaitForSeconds(buffTimer);

        if (buffApplied == true)
        {
            buffApplied = false;
            ResetBuff();
        }

    }

    // Reset values of Buff
    public void ResetBuff()
    {
        movementSpeed = movementSpeedPlaceHolder;
        currWeapon.ResetBuff();
    }

    //Armour Icon Method
    // Adds icon for everyone 1 armour the user has
    private void ArourIcons()
    {
        switch (armour)
        {
            case 0:
                ResetArmourIcons();
                break;
            case 1:
                armourIcons[0].SetActive(true);
                armourIcons[1].SetActive(false);
                armourIcons[2].SetActive(false);
                break;
            case 2:
                armourIcons[0].SetActive(true);
                armourIcons[1].SetActive(true);
                armourIcons[2].SetActive(false);
                break;
            case 3:
                armourIcons[0].SetActive(true);
                armourIcons[1].SetActive(true);
                armourIcons[2].SetActive(true);
                break;
        }
    }

    // Set aromur Icons to 0
    private void ResetArmourIcons()
    {
        for (int i = 0; i < armourIcons.Length; i++)
        {
            if (armourIcons[i] != null)
                armourIcons[i].SetActive(false);
        }
    }

    // Add to Players score
    public void AddToScore(int scoreToADD)
    {
        playerScore += scoreToADD;
    }


    // What happens when the player takes damage
    public override void TakeDamage(int dmgTaken)
    {
        // If the player has armour, lower this value first
        if (armour > 0)
            armour -= 1;
        else
            base.TakeDamage(dmgTaken);
    }

    // Calculate reamining health percentage
    // Used for health slider
    private float CalculateHealth()
    {
        return health / maxHealth;
    }

    // Get if player is dead
    public bool IsDead()
    {
        return isDead;
    }

    // Get players score
    public int Score()
    {
        return playerScore;
    }

}
