﻿/*

Camera Actor Script
10/03/2017 v1.0
Created by Michael Kijanka
Academyof interactive Entertainment

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraActor : MonoBehaviour {

    
    public Transform target; // Allows for adding player target to camera
    public float speed = 1.0f; // Speed of Camera

    private Vector3 boom;

    // Use this for initialization
    void Start () {
        // Get the Vector from the target to us
        boom = this.transform.position - target.position;
    }
	
	// Update is called once per frame
	void Update () {
        // Set our position to be the same relative to the player
        Vector3 target_pos = target.position + boom;

        this.transform.position = Vector3.Lerp(transform.position, target_pos, speed * Time.deltaTime);

    }
}
