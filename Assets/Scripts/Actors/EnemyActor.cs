﻿/*

Enemy Actor Script
13/03/2017 v1.0
Created by Michael Kijanka
Academy of Interactive Entertainment

*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


// Defines Default Zombie class

public class EnemyActor : HumanoidActor { 
        
    public float rotationSpeed; // Speed of rotation
    public float attackRate = 1.0f; // Rate the enemy can attack
    public float attackRange = 2.0f; // Range of Enemies attack
    public int scoreValue; // Score Value of this enemy

    private int scoreAdded = 0; // Check if score has been added
    private float nextAttack; // Used for time to determine when the enemy can attack next
    private float currentSpeed; // Using to take the current speed set by the user to start the Zombie moving after stopping

    [Header("Pickups")]
    public int randomThreshhold; // Change value a random number needs to beat to drop a pickup
    public GameObject []pickupObject = null; //Pickup Object

    private bool pickupDropped = false; // Determine if Zombie has already dropped a pickup

    [Header("Particle Systems")]
    public ParticleSystem bloodSplatter = null; // Zombie Blood Splatter
    
    [Header("Audio")]
    public AudioSource sfxAudioSource; // AudioSource for the Zombie
    // Audio clips
    public AudioClip sfxTakeDamage; 
    public AudioClip sfxAttack;
    public AudioClip sfxDeath = null;

    private int deathPlayCount = 0; // Using so the death audio is only played once

    [Header("Generic")]
    public PlayerActor playerTarget; // The enemy Target
    private NavMeshAgent navAgent; // Nav Agent
    private GameControl gameControl; // GameConrol for Zombie Spwan limit

    // Use this for initialization
    void Start () {
        playerTarget = GameObject.FindObjectOfType<PlayerActor>();
        animator = gameObject.GetComponent<Animator>();
        currentSpeed = movementSpeed;
        gameControl = FindObjectOfType<GameControl>();
        navAgent = GetComponent<NavMeshAgent>();
        navAgent.speed = movementSpeed;
    }
	
	// Update is called once per frame
	void Update () {
        //Check Health
        if (health <= 0)
        {
            isDead = true;
        }

        if (isDead == false)
        {
            DistanceToPlayer();
            Move();

            // Determine how far from the player the Zombie is and if in range attack
            if (DistanceToPlayer() < attackRange && Time.time > nextAttack)
            {
                Attack();
                nextAttack = Time.time + attackRate;
                animator.SetBool("bIsAttacking", true);
                navAgent.speed = 0.0f;
            }
            else if (DistanceToPlayer() >= (attackRange + 1.0f))
            {
                navAgent.destination = playerTarget.transform.position;
                navAgent.speed = movementSpeed;
                animator.SetBool("bIsAttacking", false);
            }
        }

        else
            Death();

    }

    // Enemy should move towards the player
    protected override void Move()
    {
        navAgent.destination = playerTarget.transform.position;
        
    }

    // Define how the basic Zombie attacks
    public void Attack()
    {
        playerTarget.TakeDamage(damage);

        if (sfxAudioSource != null && sfxAttack != null)
            sfxAudioSource.PlayOneShot(sfxAttack);
            
    }

    // Get the distance to Player
    // Used to Determine if Zombie is close enough to Attack
    public float DistanceToPlayer()
    {
        float distanceToPlayer; // Gets the distance to our Player
        distanceToPlayer = Vector3.Distance(this.transform.position, playerTarget.transform.position);

        return distanceToPlayer;
    }

    // What happens when the Zombie dies
    protected override void Death()
    {
        base.Death();

        if (scoreAdded == 0)
        {
            playerTarget.AddToScore(scoreValue);
            scoreAdded++;
        }

        if (sfxAudioSource != null && sfxDeath != null && deathPlayCount == 0)
        {
            sfxAudioSource.PlayOneShot(sfxDeath);
            gameControl.currentZombieCount--;
            deathPlayCount++;
        }
        navAgent.Stop();
        Invoke("DisableCollider", 1.0f);
        DropPickup();       
        Destroy(this.gameObject, 5.0f);
    }

    // What happens when the Enemy takes damage
    public override void TakeDamage(int dmgTaken)
    {

        if (sfxAudioSource != null && sfxTakeDamage != null)
            sfxAudioSource.PlayOneShot(sfxTakeDamage);

        if (bloodSplatter != null)
            bloodSplatter.Play();

        base.TakeDamage(dmgTaken);

    }

    // Function for dropping a pickup item
    private void DropPickup()
    {
        int weaponSelect; // Determines the pickup the Zombie will drop
        int randomNum; // Random number for assigning pickups

        // Should check the zombie has not already dropped a pickup, if not then rolla random number
        // If that number is higher than the threshold set by the designer drop a random pickip from
        // a collection of pickups
        if (pickupObject != null && pickupDropped == false)
        {
            Random.InitState(System.DateTime.Now.Millisecond);
            randomNum = Random.Range(1, 100);

            pickupDropped = true;

            if (randomNum > randomThreshhold)
            {
                Random.InitState(System.DateTime.Now.Millisecond);
                weaponSelect = Random.Range(0, pickupObject.Length);
                Instantiate(pickupObject[weaponSelect], this.transform.position + new Vector3(0, 2.0f, 0), Quaternion.identity);
                Debug.Log(weaponSelect);

            }
        }
        else
            Debug.Log("No Pickup Object Set");
    }

    // Disable the collider so when the zombie dies, th eplayer will not collider with the zombie
    private void DisableCollider()
    {
        this.GetComponent<CapsuleCollider>().enabled = false;
    }

}

