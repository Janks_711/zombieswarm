﻿/*

IPickupable Interface
04/04/2017 v1.0
Created by Michael Kijanka
Academy of interactive Entertainment

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This interface is used to defines what is common on all pickupable items
 * Item should be able to be picked up and adjust a property on a player
 */

interface IPickupable {

    PlayerActor playerTarget { get; set; } //Player target to adjust

    void PickedUp(); // What happens when the item is Picked up
    void ApplyBuff(); // What buff the pickup applies
}
