﻿/*

Shotgun Pickup Script
04/05/2017 v1.0
Created by Michael Kijanka
Academy of interactive Entertainment

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Weapon will switch to shotgun

public class ShotgunPickup : WeaponPickup {

public override void ApplyBuff()
    {
        playerTarget.SetWeapon(WEAPON.SHOTGUN);
    }
}
