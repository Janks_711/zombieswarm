﻿/*

Rifle Pickup Script
04/05/2017 v1.0
Created by Michael Kijanka
Academy of interactive Entertainment

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Weapon will switch to auto Rifle

public class RiflePickup : WeaponPickup {

    // Override Apply Buff method
    public override void ApplyBuff()
    {
        playerTarget.SetWeapon(WEAPON.RIFLE);

    }
}
