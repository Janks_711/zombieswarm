﻿/*

SupportPickup Script
04/04/2017 v1.0
Created by Michael Kijanka
Academy of interactive Entertainment

*/


using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Defines the base action of the support pickups
// Once player has picked up a support pickup,it should apply a buff

public class SupportPickup : MonoBehaviour, IPickupable {
    public PlayerActor playerTarget { get; set; } // Get the player target
    public int aliveTimer = 0; // How long the player has to pickup the pickup, before it is destroyed

    public virtual void ApplyBuff() { } // Function for defining a buff, Buffs to be applied in derived classes.

    // Use this for initialization
    void Start()
    {
        playerTarget = FindObjectOfType<PlayerActor>();
    }

    // Update is called once per frame
    void Update()
    {

        if (aliveTimer > 0)
        {
            this.transform.Rotate(new Vector3(0, 1, 0));
            Destroy(this.gameObject, aliveTimer); // Destroy object after specified time
        }
    }

    // Check if buff has been pickedup
    public void PickedUp()
    {
        ApplyBuff();
        playerTarget.AddToScore(5);
        Destroy(this.gameObject);
    }

    // When a player runs over the pickup,pikcup the buff
    public void OnTriggerEnter(Collider player)
    {
        if (player.tag == "Player")
        {
            PickedUp();
        }

    }
}
