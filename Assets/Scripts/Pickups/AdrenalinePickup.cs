﻿/*

Adrenaline Pickup Script
04/05/2017 v1.0
Created by Michael Kijanka
Academy of interactive Entertainment

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Players speed and fire rate will increase for a certain period of time

public class AdrenalinePickup : SupportPickup {

    public override void ApplyBuff()
    {
        playerTarget.ApplyAdrenalineBoost();
        playerTarget.AddToScore(5);
    }
}
