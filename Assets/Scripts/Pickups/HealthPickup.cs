﻿/*

Health Pickup Script
04/05/2017 v1.0
Created by Michael Kijanka
Academy of interactive Entertainment

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Player will recieve healing if required
// If full health will get additional points

public class HealthPickup : SupportPickup {

    public int healthBuff = 0; // Health to add to player

    public override void ApplyBuff()
    {
        if (playerTarget.health + healthBuff <= playerTarget.maxHealth)
            playerTarget.health += healthBuff;
        else if (playerTarget.health == playerTarget.maxHealth)
            playerTarget.AddToScore(100);
        else
            playerTarget.health += playerTarget.health % healthBuff;
    }
}
