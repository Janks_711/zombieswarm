﻿/*

Armour Pickup Script
04/05/2017 v1.0
Created by Michael Kijanka
Academy of interactive Entertainment

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// User will get one armour added to their current armour value
// If full armour will, Player will get additional points

public class ArmourPickup : SupportPickup {

    public override void ApplyBuff()
    {
        if (playerTarget.armour < 3)
        {
            playerTarget.AddToScore(5);
            playerTarget.armour += 1;
        }
        else
            playerTarget.AddToScore(100);
    }
}
