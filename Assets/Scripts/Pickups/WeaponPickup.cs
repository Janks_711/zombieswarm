﻿/*

Weapon Pickup Script
04/04/2017 v1.0
Created by Michael Kijanka
Academy of interactive Entertainment

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Define the Base class for all weapon pickups
 */

public class WeaponPickup : MonoBehaviour, IPickupable {

    public PlayerActor playerTarget { get; set; } // Get the current player
    public int aliveTimer = 0; // Set the pikcup alive timer

    public AudioSource sfxPickup = null; // Audio for the pickup

    // Use this for initialization
    void Start () {
        playerTarget = FindObjectOfType<PlayerActor>();
	}
	
	// Update is called once per frame
	void Update () {
       
        if (aliveTimer > 0)
        {
            this.transform.Rotate(new Vector3(0, 1, 0));
            Destroy(this.gameObject, aliveTimer); // Destroy object after specified time
        }
	}

    // When player passes through the pickup, the weapon switches
    public void OnTriggerEnter(Collider player)
    {
        if (player.tag == "Player")
        {
            PickedUp();
        }
            
    }

    // When picked up Destoryed pickup object and change Players Weapon type
    public void PickedUp()
    {
        ApplyBuff();
        playerTarget.AddToScore(5);
        Destroy(this.gameObject);
    }

    public virtual void ApplyBuff() { } // Apply Buff to the player

  }
