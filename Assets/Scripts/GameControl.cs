﻿/*

GameControl Script
16/05/2017 v1.0
Created by Michael Kijanka
Academyof interactive Entertainment

*/

// This class will be used to control the members as well as other game control neccessities like, limiting the amount of zombies that can be spawned

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour {
    
    public PlayerActor playerTarget; // Get Target Player
    public Text scoreText; // Text to apply score when character dies
    public AudioSource backgroundMusic; //Background music for the game

    [Header("Menu Objects")]
    public GameObject pauseMenu; // Pause Menu Object
    public GameObject startMenu; // Start Menu Object
    public GameObject deathMenu; // Death Menu Object
    private bool paused = false; // Check if game is paused
    private bool menuOpen = false;// Check if menu is open
    private GameObject currentMenu; // return the current menu that is open

    [Header("MobilePause")]
    public GameObject mobilePauseButton; // Pause button for mobile

    [Header("Zombie Control")]
    public int maxZombieCount = 6; // The max zombie count
    public int currentZombieCount = 0; //The current number of zombies in game

	// Use this for initialization
	void Start () {

        playerTarget = FindObjectOfType<PlayerActor>();
    
        menuOpen = false; // Initially set to close,this will go true in the StartMenuFucntion()

        if (backgroundMusic != null)
            backgroundMusic.Stop();

        // Set pause and Death Menus to close
        if (pauseMenu != null)
            pauseMenu.SetActive(false);

        if (deathMenu != null)
            deathMenu.SetActive(false);

        // The game will start with the Start Menu open
        if (startMenu != null && menuOpen == false)
            StartMenu(startMenu);

        // Hide mobile pause button if not in Android Build
     #if !UNITY_ANDROID
        if (mobilePauseButton.gameObject == null)
            Debug.Log("You forgot to attach the joysticks!");
        else
        {
           mobilePauseButton.gameObject.SetActive(false);
            
        }
    #endif

    }
	
	// Update is called once per frame
	void Update () {

        // Open or close pause menu if escape key is pressed
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (pauseMenu.activeSelf == false && menuOpen == false)
                StartMenu(pauseMenu);
            else if (pauseMenu.activeSelf == true && menuOpen == true)
                CloseMenu(pauseMenu);
        }

        //Game will stop if pause is true
        if (paused == true)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;

        // Once player dies, start End Game function
        if (playerTarget.IsDead() == true)
        {
            mobilePauseButton.SetActive(false);
            StartCoroutine(EndGame());
        }
    }

    // Check if gamne is paused
    public bool IsGamePaused()
    {
        return paused;
    }

    // Open Pause Menu
    public void StartMenu(GameObject menu)
    {
        currentMenu = menu;

        menuOpen = true;
        paused = true;
        menu.SetActive(true);
           
    }

    // Close Menu
    public void CloseMenu(GameObject menu)
    {
        paused = false;
        menuOpen = false;
        menu.SetActive(false);

    }

    //Resume Game
    public void Resume()
    {
        if (paused)
            paused = false;

        if (currentMenu.activeSelf == true)
        {
            currentMenu.SetActive(false);
            menuOpen = false;
        }
            
    }

    // Restart Scene
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    //Quit Game
    public void QuitApplication()
    {
        Application.Quit();
    }


    // Game Over
    public void GameOver()
    {
        if (deathMenu != null && menuOpen == false)
            StartMenu(deathMenu);
    }

    // Wait a couple of seconds after the player dies then open the Player detah Menu
    IEnumerator EndGame()
    {
        yield return new WaitForSeconds(3.0f);

        StartMenu(deathMenu);

        if (scoreText != null)
            scoreText.text = "Congratulations you scored\n" + playerTarget.Score();
    }

    // Start Background Music
    public void StartMusic()
    {
        if(backgroundMusic != null)
            backgroundMusic.Play();
    }

    // Toggle if music is on or off
    public void MusicToggle()
    {
        if (backgroundMusic.mute == false)
            backgroundMusic.mute = true;
        else if (backgroundMusic.mute == true)
            backgroundMusic.mute = false;
    }
}
