﻿/*

Pistol Script
30/04/2017 v1.0
Created by Michael Kijanka
Academy of interactive Entertainment

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Defines how the pistol will shoot
// This will temporarily serve as the auto rifle scriptas well, as the only things that will need to change are
// - Rate of fire
// - Sound of the gun

public class Pistol : Weapon {

    // Fire single Ray straight ahead
    public override void FireType()
    {
        // Create RayCast straight ahead
        Ray fireRay = new Ray(transform.position, transform.forward); ; // Ray for firing shot
        RaycastHit hit; // Get information when Ray hits a target

        if (gunMuzzle != null)
            fireRay = new Ray(gunMuzzle.transform.position, gunMuzzle.transform.forward);
        else

                Debug.Log("Set Gun Muzzle Transform!!");

        // Determine if hit an enemy
        if (Physics.Raycast(fireRay, out hit, 50.0f))
        {
            Debug.Log("Ray hit" + hit.ToString());
            if (hit.collider.tag == "Enemy")
            {
                hit.collider.gameObject.GetComponent<EnemyActor>().TakeDamage(damage);
            }
        }

        // Apply gun Audio
        if (sfxShoot != null && sfxcasing != null)
            {
                sfxShoot.Play();
                sfxcasing.PlayDelayed(0.3f);
            }
        // Apply Particle animations
        if (bulletTrail != null && muzzleflash != null)
        {
            bulletTrail.Play();
            muzzleflash.Play();
        }
    }

}
