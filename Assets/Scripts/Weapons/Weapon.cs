﻿/*

Weapon Script
01/05/2017 v1.0
Created by Michael Kijanka
Academy of interactive Entertainment

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Defines the base class for all weapons in the game

public abstract class Weapon : MonoBehaviour {

    [Header("Gun Stats")]
    public int damage; // Sets weapon Damage
    public int maxAmmo; // Maximumammo for the current weapon
    public float fireRate = 0.5f; // Fire rate of the weapon

    [HideInInspector]
    public float Nextfire { get { return nextfire; } set { nextfire = value; } } //To time fire on Player Actor

    private int ammo = -1; // Current Ammo, -1 means unlimited

    // Used as placeholders for gun reset
    protected float fireRatePlaceHolder;
    protected float nextfire;
    
    [Header("Position")]
    public PlayerActor playerTarget;
    public Transform gunMuzzle; // Set start point of ray

    [Header("Particle Systems")]
    public ParticleSystem bulletTrail; // Bullet Trail particle system
    public ParticleSystem muzzleflash; // Set Muzzle Flash Image

    [Header("Audio Sources")]
    public AudioSource sfxShoot = null; // Shot Sound
    public AudioSource sfxcasing = null; // Bullet casing sound
    public AudioSource sfxreload = null; // reload sound

    [Header("User Interface")]
    public Sprite weapoonImage = null; // Image of the gun, to use on UI

   private void Start()
    {
        playerTarget = GameObject.FindObjectOfType<PlayerActor>();
        fireRatePlaceHolder = fireRate;
    }

    // Every Weapons must have a Fire function
    public abstract void FireType();

    // When weapon is fired,.
    // If no ammo left, reset to pistol
    public void Fire()
    {
        if (ammo != 0)
        {
            FireType();
            ammo--;
        }
        else
            playerTarget.SetWeapon(WEAPON.PISTOL);

        Debug.Log("Ammo Left: " + ammo);
    }

    public void Reload() // reset ammo to full
    {
            ammo = maxAmmo;
    }

    // Return remaining ammo
    public int AmmoRemaining()
    {
        return ammo;
    }

    // This is to be used to reset values if Buff has been applied
    public void ResetBuff()
    {
        fireRate = fireRatePlaceHolder;
    }
}
