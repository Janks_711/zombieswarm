﻿/*

Shotgun Script
01/05/2017 v1.0
Created by Michael Kijanka
Academy of interactive Entertainment


*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Shotgun shouldshoot scattered rays, like the pellets out of a shotgun shell

public class Shotgun : Weapon {

    [Header("Shotgun Spread")]
    public int bulletCount = 10; // Number of shutgun pellets
    public float variance = 0.5f; // Variance of spread
    Ray fireRay; // Ray of pellet

    public override void FireType()
    {
        // For each pellet
        // - Offset the current pellet
        // - Check if pellet hits enemy
        for (int i = 0; i < bulletCount; i++)
        {
            if (gunMuzzle != null)
            {
                Vector3 offset = transform.up * UnityEngine.Random.Range(0.0f, variance); // Off set the ray so that it does not fire straight ahead, 
                                                                                          // uses variance to determine how far the ray can be offset
                offset = Quaternion.AngleAxis(UnityEngine.Random.Range(0.0f, 360.0f), gunMuzzle.transform.forward) * offset;
                
                fireRay = new Ray(gunMuzzle.transform.position, gunMuzzle.transform.forward + offset); // Ray for firing shot
                Debug.DrawRay(gunMuzzle.transform.position, gunMuzzle.transform.forward + offset, Color.red, 10.0f);
            }
            else
                Debug.Log("Set Gun Muzzle Transform!!");

            RaycastHit hit; // Get information when Ray hits a target

            // Check if pellet hits enemy
            if (Physics.Raycast(fireRay, out hit, 50.0f))
            {
                Debug.Log("Ray hit" + hit.ToString());
                if (hit.collider.tag == "Enemy")
                {
                    hit.collider.gameObject.GetComponent<EnemyActor>().TakeDamage(damage);
                }
            }
           
        }

        // Apply sounds with gun fire
        if (sfxShoot != null && sfxcasing != null)
        {
            sfxShoot.Play();
            sfxcasing.PlayDelayed(0.3f);
        }

        // Apply particle system animations
        if (bulletTrail != null && muzzleflash != null)
        {
            bulletTrail.Play();
            muzzleflash.Play();
        }
    }
}

