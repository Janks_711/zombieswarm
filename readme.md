## Zome Swarm

This is a Zombie shooter game made on the Unity 3D game engine. This was made during my time at AIE.

At the time of making this game, the game was made using Perforce for source control on the AIE servers.

This has been moved to git for my portfolio website.